package models;

import helper.datasources.MorphiaObject;
import org.bson.types.ObjectId;
import org.mongodb.morphia.annotations.Entity;
import org.mongodb.morphia.annotations.Id;
import org.mongodb.morphia.annotations.Indexed;
import org.mongodb.morphia.query.Query;
import play.data.format.Formats;
import play.data.validation.Constraints;

import java.util.Date;
import java.util.List;

/**
 * Created by SiddhuJz on 10-04-2015.
 */

@Entity
public class CuratedCast {
    /**
     *
     */
    private static final long serialVersionUID = 1L;

    @Id
    public ObjectId id;

    @Indexed(unique = true)
    public String ccid;

    @Formats.DateTime(pattern = "yyyy-MM-dd HH:mm:ss")
    public Date datePublished;
    @Formats.DateTime(pattern = "yyyy-MM-dd HH:mm:ss")
    public Date date_created;
    public String date_created_str;

    public String title;
    public String shortDescription;
    public String fullDescription;
    public String imageUrl;
    public String source;
    public String sourceUrl;
    public String author;
    public String mediaTypeRank;
    public double upVotes;
    public double downVotes;
    public boolean isVerified;

    public static void create(CuratedCast curatedCast) {
        MorphiaObject.datastore.save(curatedCast);
    }

    public static List<CuratedCast> getLatestByPage(Integer pgn) {
        return getLatestCuratedCastByPage(pgn).asList();
    }

    private static Query<CuratedCast> getLatestCuratedCastByPage(Integer pgn) {
        //var posts = db.find({}).sort({ ts: -1 }).limit(20)
        //var last_ts = posts[posts.length-1]['ts']['sec'];
        //var posts = db.find({ ts: {$gt: {last_ts}}}).sort({ ts: -1 }).limit(20);

        return MorphiaObject.datastore.find(CuratedCast.class).offset((pgn-1) * 10).limit(10);
    }

    public static CuratedCast getByCcid(String ccid) {
        return getCuratedCastByCcid(ccid).get();
    }

    private static Query<CuratedCast> getCuratedCastByCcid(final String ccid) {
        return MorphiaObject.datastore.createQuery(CuratedCast.class).filter("isVerified", true)
                .filter("ccid", ccid);
    }

}
