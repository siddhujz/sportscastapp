/**
 * Created by ntenisOT on 16/10/14.
 */


import actors.HelloActor;
import akka.actor.ActorRef;
import akka.actor.Cancellable;
import akka.actor.Props;
import com.feth.play.module.pa.PlayAuthenticate;
import com.feth.play.module.pa.PlayAuthenticate.Resolver;
import com.feth.play.module.pa.exceptions.AccessDeniedException;
import com.feth.play.module.pa.exceptions.AuthException;
import controllers.routes;
import helper.datasources.MongoDB;
import helper.datasources.MorphiaObject;
import models.SecurityRole;
import play.Application;
import play.GlobalSettings;
import play.Logger;
import play.libs.Akka;
import play.mvc.Call;
import scala.concurrent.duration.Duration;

import java.util.Arrays;
import java.util.concurrent.TimeUnit;

public class Global extends GlobalSettings {

    public static Logger.ALogger globalLogger = Logger.of("application.Global");
    private Cancellable scheduler;

    public void onStart(Application app) {
        if(globalLogger.isInfoEnabled()) {
            globalLogger.info("SportsCastApp Application started");
        }

        MongoDB.connect();

        Logger.info("Connected to Database!");

        PlayAuthenticate.setResolver(new Resolver() {

            @Override
            public Call login() {
                // Your login page
                return routes.Application.login();
            }

            @Override
            public Call afterAuth() {
                // The user will be redirected to this page after authentication
                // if no original URL was saved
                return routes.Application.index();
            }

            @Override
            public Call afterLogout() {
                return routes.Application.index();
            }

            @Override
            public Call auth(final String provider) {
                // You can provide your own authentication implementation,
                // however the default should be sufficient for most cases
                return com.feth.play.module.pa.controllers.routes.Authenticate
                        .authenticate(provider);
            }

            @Override
            public Call askMerge() {
                return routes.Account.askMerge();
            }

            @Override
            public Call askLink() {
                return routes.Account.askLink();
            }

            @Override
            public Call onException(final AuthException e) {
                if (e instanceof AccessDeniedException) {
                    return routes.Signup
                            .oAuthDenied(((AccessDeniedException) e)
                                    .getProviderKey());
                }

                // more custom problem handling here...
                return super.onException(e);
            }
        });

        initialData();

        schedule();
    }

    public void onStrop(Application app) {
        if(globalLogger.isInfoEnabled()) {
            globalLogger.info("SportsCastApp Application stopped!");
        }
        //Stop the scheduler
        if (scheduler != null) {
            scheduler.cancel();
            this.scheduler = null;
        }
        MongoDB.disconnect();
    }

    // Update to add a role
    private void initialData() {

        if (MorphiaObject.datastore.createQuery(SecurityRole.class).countAll() == 0) {
            for (final String roleName : Arrays
                    .asList(controllers.Application.USER_ROLE)) {
                final SecurityRole role = new SecurityRole();
                role.roleName = roleName;
                MorphiaObject.datastore.save(role);
            }
        }
    }

    private void schedule() {
        try {
            ActorRef helloActor = Akka.system().actorOf(Props.create(HelloActor.class));
            //ActorRef helloActor = Akka.system().actorOf(new Props(HelloActor.class));
            scheduler = Akka.system().scheduler().schedule(
                    Duration.create(0, TimeUnit.MINUTES), //Initial delay 10 minutes
                    Duration.create(30, TimeUnit.MINUTES),     //Frequency 30 minutes
                    helloActor,
                    "hello",
                    Akka.system().dispatcher(), null);
        }catch (IllegalStateException e){
            //Caught every time the CronExpression is modified
            if(globalLogger.isErrorEnabled()) {
                globalLogger.error("Error caused by reloading application!", e);
            }
        }catch (Exception e) {
            if(globalLogger.isErrorEnabled()) {
                globalLogger.error("Error!", e);
            }
        }
    }
}
