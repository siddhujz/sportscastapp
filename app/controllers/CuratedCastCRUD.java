package controllers;

import models.CuratedCast;
import play.libs.F;
import play.libs.F.Promise;
import play.libs.F.Function0;
import play.mvc.Result;
import views.html.curated_cast_post;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.util.List;

import static play.mvc.Results.ok;


/**
 * Created by SiddhuJz on 16-04-2015.
 */
public class CuratedCastCRUD {

    public static Promise<Result> getPost(final String ccid) {
        Promise<CuratedCast> curatedCastPromise = Promise.promise(
                new Function0<CuratedCast>() {
                    public CuratedCast apply() throws UnsupportedEncodingException {
                        return CuratedCast.getByCcid(URLDecoder.decode(ccid, "UTF-8"));
                    }
                }
        );
        return curatedCastPromise.map(
                new F.Function<CuratedCast, Result>() {
                    public Result apply(CuratedCast curatedCast) {
                        return ok(curated_cast_post.render(curatedCast));
                    }
                }
        );
    }


}
