package controllers;

import com.google.gson.Gson;
import com.mongodb.util.JSON;
import models.CuratedCast;
import org.apache.commons.lang3.StringUtils;
import play.mvc.Result;

import java.util.ArrayList;
import java.util.List;

import static play.mvc.Results.ok;

/**
 * Created by SiddhuJz on 13-04-2015.
 */
public class ApiCRUD {

    public static Result latestCuratedCast(String pgn) {
        //If the pgn passed is not an Integer, then assign 1 to pgn
        if (!StringUtils.isNumeric(pgn)) {
            pgn = "1";
        }
        final Integer page = Integer.parseInt(pgn);

        List<CuratedCast> curatedCastList = new ArrayList<>();
        curatedCastList = CuratedCast.getLatestByPage(page);

        Gson gson = new Gson();
        String curatedCastListJson = gson.toJson(curatedCastList);
        return ok(curatedCastListJson);
    }
}
