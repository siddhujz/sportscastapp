package actors;

import akka.actor.UntypedActor;
import models.CuratedCast;
import play.Logger;
import utilities.Utilities;

import java.util.List;

/**
 * Created by SiddhuJz on 10-04-2015.
 */
public class HelloActor extends UntypedActor {

    public static Logger.ALogger helloActorLogger = Logger.of("application.actors.HelloActor");

    @Override
    public void onReceive(Object message) throws Exception {
        // This method will be called when ever the job runs.
        if(helloActorLogger.isDebugEnabled()) {
            helloActorLogger.debug("Calling Curate data");
        }
        if (message.equals("hello")) {
            // controllers.Application.log();
            List<CuratedCast> curatedCastList = Utilities.getCuratedCast("");
            if (curatedCastList != null && curatedCastList.size() > 0) {
                for (int curatedCastIndex = 0; curatedCastIndex < curatedCastList.size(); curatedCastIndex++) {
                    // TODO - Remove comments before production
                    // CuratedCast.create(curatedCastList.get(curatedCastIndex));
                }
            }
            System.out.println("----------------------done---------------------");
        } else {
            unhandled(message);
        }
    }
}