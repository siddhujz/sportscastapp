package utilities;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import extras.Constants;
import models.CuratedCast;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.select.Elements;
import play.Logger;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by SiddhuJz on 11-04-2015.
 */
public class Utilities {

    public static Logger.ALogger utilitiesLogger = Logger.of("application.utilities.Utilities");

    public static String normalize(String in) {
        return in.toLowerCase().replaceAll("[-,!'\" ]", "");
    }

    public static String getDomainName(String url) throws URISyntaxException {
        if(!url.startsWith("http") && !url.startsWith("https")){
            url = "http://" + url;
        }
        if (url.equals("No valid link given")) {
            return "unknown";
        }
        URI uri = new URI(url);
        String domain = uri.getHost();
        if(domain == null) {
            if(url.contains("blogger")) {
                System.out.println("Couldn't fetch the domain through code automation. Since the URL contains blogger, domain is being hardcoded to blooger.com");
                return "blogger.com";
            }
            System.out.println("NULL domain detected in URI :" + uri);
            return "unknown";
        }
        return  domain.startsWith("www.") ? domain.substring(4) : domain;
    }

    public static String getFinalRedirectedUrl(String url) {
        HttpURLConnection connection;
        String finalUrl = url;
        try {
            do {
                connection = (HttpURLConnection) new URL(finalUrl).openConnection();
                connection.setInstanceFollowRedirects(false);
                connection.setUseCaches(false);
                connection.setRequestMethod("GET");
                connection.connect();
                int responseCode = connection.getResponseCode();
                if (responseCode >= 300 && responseCode < 400) {
                    String redirectedUrl = connection.getHeaderField("Location");
                    if (null == redirectedUrl)
                        break;
                    finalUrl = redirectedUrl;
                    System.out.println("redirected url: " + finalUrl);
                } else
                    break;
            } while (connection.getResponseCode() != HttpURLConnection.HTTP_OK);
            connection.disconnect();
            return finalUrl;
        } catch (Exception e) {
            e.printStackTrace();
            return url;
        }
    }

    public static List<CuratedCast> getCuratedCast(String pipeUrl) {
        //Execution start time
        long exeStartTime = new Date().getTime();

        if(utilitiesLogger.isDebugEnabled()) {
            utilitiesLogger.debug("Entered getCuratedCast()");
        }
        //Constants used
        //TODO - noOfItemsNeeded has to be changed in Production
        int noOfItemsNeeded = 1;
        int descriptionPreLimit = 250;
        int descriptionPostLimit = 10000;
        String defaultDescription = "This link is the new trending and the one of the most visited, looked at by the world ";
        String mediaTypeRankYahooPipeDefault = "blog";
        String defaultImageURL = Constants.DEFAULT_YAHOO_PIPE_IMAGE_URL;   //Used in case a blog doesn't have any image in it
        String yahooPipeURL = Constants.DEFAULT_YAHOO_PIPE_URL;
        if (pipeUrl != null && !pipeUrl.isEmpty()) {
            yahooPipeURL = pipeUrl;
        }
        //Objects used for data transimission
        List<CuratedCast> curatedCastList = new ArrayList<>();

        try {
            if(utilitiesLogger.isDebugEnabled()) {
                utilitiesLogger.debug("Sending a HTTP GET request");
            }
            URL urlObject = new URL(yahooPipeURL);     //Sending a HTTP GET request
            if(utilitiesLogger.isDebugEnabled()) {
                utilitiesLogger.debug("Opening HttpURLConnection");
            }
            HttpURLConnection clientRequest = (HttpURLConnection) urlObject.openConnection();
            if(utilitiesLogger.isDebugEnabled()) {
                utilitiesLogger.debug("Setting RequestMethod to GET");
            }
            clientRequest.setRequestMethod("GET");
            if(utilitiesLogger.isDebugEnabled()) {
                utilitiesLogger.debug("Getting ResponseCode");
            }
            int responseCode = clientRequest.getResponseCode();
            //Interested variables used for saving extracted data
            String imageURL = "";
            String moreLink;
            String description;
            String title,pubDate,author = "";
            String sourceDomain;
            String parsedDescription;

            //External Helper Objects
            SimpleDateFormat destFormatter = new SimpleDateFormat("dd/MM/yyyy");  //formats to our followed nomenclature

            System.out.println("\nResponse HTTP code is :" + responseCode);
            if (responseCode == 200) {
                InputStreamReader inputStreamReader = new InputStreamReader(clientRequest.getInputStream());
                BufferedReader isReader = new BufferedReader(inputStreamReader);
                JsonParser parserObj = new JsonParser();
                StringBuilder jsonSB = new StringBuilder();
                String line;
                while ( (line = isReader.readLine()) != null) {
                    jsonSB.append(line + "\n");
                }
                isReader.close();
                System.out.println("Json Response: Execution time is " + (new Date().getTime() - exeStartTime));
                JsonElement responseJsonElement = parserObj.parse(jsonSB.toString());
                //JsonElement responseJsonElement = parserObj.parse(isReader);
                JsonObject responseJsonObject = responseJsonElement.getAsJsonObject();

                JsonArray itemsJsonArray=responseJsonObject.getAsJsonObject("value").getAsJsonArray("items");
                JsonObject tempJsonObject;
                int jsonObjectsAvailable = itemsJsonArray.size();

                //Total Execution time = Execution start time - Execution end time
                long curExeEndTime = new Date().getTime();
                long curExeTime = curExeEndTime - exeStartTime;
                System.out.println("Current Execution time is " + curExeTime);

                for(int i = 0;(i < noOfItemsNeeded) && (i < jsonObjectsAvailable); i++)
                {
                    int missedCount = 0;  //Has a count how many required fileds by curated content are missing in the source given
                    JsonElement tempJsonElement = itemsJsonArray.get(i);
                    tempJsonObject = tempJsonElement.getAsJsonObject();
                    if(tempJsonObject.get("title")!= null) {
                        title = tempJsonObject.get("title").toString();
                    } else if (tempJsonObject.get("y:title") != null){
                        title = tempJsonObject.get("y:title").toString();
                    } else {
                        missedCount++;
                        title = "Interior design article";
                    }
                    title = title.replace("\\\"", "");

                    if(tempJsonObject.get("pubDate") != null) {
                        pubDate = tempJsonObject.get("pubDate").getAsString();
                        //EE, dd MMM yyyy HH:mm:ss z  = Sat Jun 01 12:53:10 IST 2013
                        SimpleDateFormat formatter = new SimpleDateFormat("EE, dd MMM yyyy HH:mm:ss");
                        try {
                            Date parsedDate = formatter.parse(pubDate);
                            pubDate = destFormatter.format(parsedDate);
                        } catch (ParseException parseexception) {  //If date sent is not in parseable format then set today's date
                            Date today = new Date();
                            pubDate = destFormatter.format(today);
                        }
                    } else {    // If the published date given by them is NULL, assign to todays date
                        Date today = new Date();
                        pubDate = destFormatter.format(today);
                    }

                    if (tempJsonObject.get("link") != null) {
                        moreLink = tempJsonObject.get("link").getAsString();
                    } else if (tempJsonObject.get("feedburner:origlink") != null){
                        moreLink = tempJsonObject.get("feedburner:origlink").getAsString();
                    } else if(tempJsonObject.get("guid")!= null && tempJsonObject.get("guid").getAsJsonObject().get("content") != null) {
                        moreLink = tempJsonObject.get("guid").getAsJsonObject().get("content").getAsString();
                    } else {
                        moreLink = "Novalidlinkgiven";   //This string is hardcoded and used  in Utilities class
                        missedCount++;
                    }

                    String sourceHelper = "";
                    if (tempJsonObject.has("comments")) {
                        sourceHelper = tempJsonObject.get("comments").getAsString();
                    } else if (tempJsonObject.get("link") != null) {
                        sourceHelper = tempJsonObject.get("link").getAsString();
                    } else if (tempJsonObject.get("feedburner:origlink") != null){
                        sourceHelper = tempJsonObject.get("feedburner:origlink").getAsString();
                    } else if(tempJsonObject.get("guid")!= null && tempJsonObject.get("guid").getAsJsonObject().get("content") != null && !(tempJsonObject.get("guid").getAsJsonObject().get("content").getAsString().contains("blogger.com")) ){
                        sourceHelper = tempJsonObject.get("guid").getAsJsonObject().get("content").getAsString();
                    } else {
                        sourceHelper = "Novalidlinkgiven";   //This string is hardcoded and used  in Utilities class
                        missedCount++;
                    }
                    //System.out.println(curatedCastList.size() + " - Source link helper is :" + sourceHelper);

                    if(tempJsonObject.has("description") && !tempJsonObject.get("description").isJsonNull()) {
                        description = tempJsonObject.get("description").getAsString();
                    } else if (tempJsonObject.get("content:encoded") != null){
                        if (tempJsonObject.get("content:encoded").isJsonPrimitive()) {
                            description = tempJsonObject.get("content:encoded").getAsString();
                        } else {
                            description = tempJsonObject.get("content:encoded").getAsJsonObject().get("content").getAsString();
                        }
                    } else {
                        description = defaultDescription;
                        missedCount++;
                    }

                    if(tempJsonObject.get("author")!= null)
                    {
                        JsonObject authortemp = null;
                        if(tempJsonObject.get("author").isJsonObject()) {
                            authortemp = tempJsonObject.get("author").getAsJsonObject();
                            author = authortemp.get("name").getAsString();
                        } else {
                            author = tempJsonObject.get("author").toString();
                        }
                    } else if (tempJsonObject.get("dc:creator") != null)
                    {
                        if (tempJsonObject.get("dc:creator").isJsonPrimitive()) {
                            author = tempJsonObject.get("dc:creator").getAsString();
                        } else {
                            author = tempJsonObject.get("dc:creator").getAsJsonObject().get("content").getAsString();
                        }
                    } else {
                        author = "Author unknown";
                        missedCount++;
                    }

                    try {
                        sourceDomain = Utilities.getDomainName(sourceHelper);
                    } catch (URISyntaxException uriSyntaxException) {
                        //System.out.println("Exception raised while getting the source domain of a URL");
                        uriSyntaxException.printStackTrace();
                        continue;
                    }
                    Document doc = Jsoup.parse(description);
                    parsedDescription = doc.text();             //Jsoup.parse(description).text();

                    Elements images = doc.select("img[src~=(?i)\\.(png|jpe?g|gif)]"); //(description,"img[src~=(?i)\\.(png|jpe?g|gif)]");
                    //System.out.println(curatedCastList.size() + " - Title : " + title);
                    if (images.size() == 0 || images.get(0).attr("src").contains("rss.feedsportal")) {
                        imageURL = defaultImageURL;
                        missedCount++;
                    } else if(images.size() > 0) {
                        imageURL = images.get(0).attr("src");
                    }
                    //If more than 3 required fields are not given in the source- skip the source element
                    if (missedCount >= 3) {
                        //System.out.println("Skipping an element due to inadequate fields");
                        continue;
                    }

                    int endIndex;  //Stores the end Index till Descriptionm has to be stored
                    if(parsedDescription.length() > descriptionPreLimit) {
                        String tailString = (parsedDescription.length() >= descriptionPostLimit)?parsedDescription.substring(descriptionPreLimit,descriptionPostLimit):parsedDescription.substring(descriptionPreLimit);
                        if(tailString.contains(".")) {
                            endIndex = tailString.lastIndexOf(".");
                            endIndex += descriptionPreLimit;
                            parsedDescription = parsedDescription.substring(0,endIndex);
                        }
                        else if (tailString.contains("...")) {
                            endIndex = tailString.indexOf("...");
                            endIndex += descriptionPreLimit;
                            parsedDescription = parsedDescription.substring(0,endIndex) + "...";
                        }
                        else {
                            endIndex = (parsedDescription.length() <= descriptionPostLimit)?parsedDescription.length():descriptionPostLimit;
                            parsedDescription = parsedDescription.substring(0,endIndex) + "...";
                            //System.out.println("The string length for" + title + " is more than prelimit and doesn't have ends : " + parsedDescription.length());
                        }
                    }

                    //Get the final redirect url
                    moreLink = getFinalRedirectedUrl(moreLink);

                    CuratedCast curatedCast = new CuratedCast();
                    curatedCast.ccid = Utilities.normalize(moreLink);
                    //Date at which the blog is created
                    curatedCast.date_created = destFormatter.parse(pubDate); //String --> Date obj --> Long (milliseconds)
                    curatedCast.date_created_str = pubDate;
                    //The below is the date at which we have published onto our platform
                    curatedCast.datePublished = new Date();

                    curatedCast.title = title;
                    curatedCast.shortDescription = parsedDescription;
                    curatedCast.fullDescription = parsedDescription;
                    curatedCast.imageUrl = imageURL;
                    curatedCast.source = sourceDomain;
                    curatedCast.sourceUrl = moreLink;
                    curatedCast.author = author;
                    curatedCast.mediaTypeRank = mediaTypeRankYahooPipeDefault;
                    curatedCast.isVerified = true;

                    curatedCastList.add(curatedCast);
                }
            }
            //TODO - Insert into database here.
            if(utilitiesLogger.isDebugEnabled()) {
                utilitiesLogger.debug("curatedCastList.size() = " + curatedCastList.size());
            }
        }
        catch (Exception e) {
            System.out.println(e.getMessage());
            e.printStackTrace();
        }

        //Total Execution time = Execution start time - Execution end time
        long exeEndTime = new Date().getTime();
        long exeTime = exeEndTime - exeStartTime;
        if(utilitiesLogger.isDebugEnabled()) {
            utilitiesLogger.debug("Execution time is " + exeTime);
        }
        System.out.println("SportsCast - 0 : " + curatedCastList.get(0).toString());
        return curatedCastList;
    }
}
